from __future__ import division

import Part
import math as mt
from FreeCAD import Base
from FreeCAD import Gui

def PerfSheet(App,L,H,W,R,Type="circle", s=1/5):
  HoleSK=App.ActiveDocument.addObject('Sketcher::SketchObject','SheetPerforationHoles')
  HoleSK.Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(0.000000,0.000000,0.000000,1.000000))
  SheetSK=App.ActiveDocument.addObject('Sketcher::SketchObject','SheetBase')
  SheetSK.Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(0.000000,0.000000,0.000000,1.000000))
  SheetSK.addGeometry(Part.LineSegment(App.Vector(0,0,0),App.Vector(0,H,0)))
  SheetSK.addGeometry(Part.LineSegment(App.Vector(0,H,0),App.Vector(L,H,0)))
  SheetSK.addGeometry(Part.LineSegment(App.Vector(L,H,0),App.Vector(L,0,0)))
  SheetSK.addGeometry(Part.LineSegment(App.Vector(L,0,0),App.Vector(0,0,0)))
  e = R*s
  X_number = int( mt.ceil(L / (e+2*R) ))
  Y_number = int( mt.ceil(H / (e+2*R) ))
  
  if Type=="circle":
    Step_X =( e + 2*R )
    Step_Y = Step_X
    for i in range( X_number+1):
      PositionX = Step_X*i
      if i % 2 != 0: #This creates zig-zag pattern
        Y_Zero = Step_Y*0.5
      else:
        Y_Zero = 0
      for n in range( Y_number+1):
        PositionY = Step_Y*n+Y_Zero
        HoleSK.addGeometry(Part.Circle(App.Vector(PositionX, PositionY, 0), App.Vector(0,0,1),R))
  
  if Type=="square":
    Step_X =( e + 2*R )
    Step_Y = Step_X
    Cosine = mt.cos(mt.radians(45))
    Sine = mt.cos(mt.radians(45))
    for i in range( X_number + 1 ):
      PositionX = Step_X*i
      for n in range( Y_number + 1):
        PositionY = Step_Y*n
        A = App.Vector(PositionX + R, PositionY - R, 0)
        B = App.Vector(PositionX + R, PositionY + R, 0)
        C = App.Vector(PositionX - R, PositionY + R, 0)
        D = App.Vector(PositionX - R, PositionY - R, 0)
        HoleSK.addGeometry(Part.LineSegment(A,B))
        HoleSK.addGeometry(Part.LineSegment(B,C))
        HoleSK.addGeometry(Part.LineSegment(C,D))
        HoleSK.addGeometry(Part.LineSegment(D,A))
    
  if Type=="hexagon":
    Step_X =( e + 2*mt.cos(mt.radians(30))*R )
    Step_Y = ( e + 2*R )

    for i in range( X_number+1):
      PositionX = Step_X*i
      if i % 2 != 0: #This creates zig-zag pattern
        Y_Zero = Step_Y*0.5
      else:
        Y_Zero = 0
      for n in range( Y_number+1):
        PositionY = Step_Y*n+Y_Zero
        #build hexagon
        #Points
        Cosine = mt.cos(mt.radians(60))
        Sine = mt.sin(mt.radians(60))
        A = App.Vector(PositionX + R, PositionY, 0)
        B = App.Vector(PositionX + R*Cosine, PositionY + R*Sine, 0)
        C = App.Vector(PositionX - R*Cosine, PositionY + R*Sine, 0)
        D = App.Vector(PositionX - R, PositionY, 0)
        E = App.Vector(PositionX - R*Cosine, PositionY - R*Sine, 0)
        F = App.Vector(PositionX + R*Cosine, PositionY - R*Sine, 0)
        HoleSK.addGeometry(Part.LineSegment(A,B))
        HoleSK.addGeometry(Part.LineSegment(B,C))
        HoleSK.addGeometry(Part.LineSegment(C,D))
        HoleSK.addGeometry(Part.LineSegment(D,E))
        HoleSK.addGeometry(Part.LineSegment(E,F))
        HoleSK.addGeometry(Part.LineSegment(F,A))
  
  PerfS = App.ActiveDocument.addObject("PartDesign::Pad","holes")
  PerfS.Sketch = HoleSK
  PerfS.Length = W
  SheetS = App.ActiveDocument.addObject("PartDesign::Pad","BaseSheet")
  SheetS.Sketch = SheetSK
  SheetS.Length = W
  PerforatedSheet = App.ActiveDocument.addObject("Part::Cut", "PerforatedSheet")
  PerforatedSheet.Base = SheetS   
  PerforatedSheet.Tool = PerfS
  
  HoleSKguiname = HoleSK.Label
  Gui.App.ActiveDocument.getObject(HoleSKguiname).Visibility = False
  SheetSKguiname = SheetSK.Label
  Gui.App.ActiveDocument.getObject(SheetSKguiname).Visibility = False
  App.ActiveDocument.recompute()
